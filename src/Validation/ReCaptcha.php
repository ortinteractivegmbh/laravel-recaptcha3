<?php

namespace ORT\Interactive\Recaptcha3\Validation;

use GuzzleHttp\Client;

class ReCaptcha
{

    public function validate($attribute,
                             $value,
                             $parameters,
                             $validator)
    {
        $score = (float)config('services.google.recaptcha3.score');

        $url = sprintf(
            'https://www.google.com/recaptcha/api/siteverify?%s',
            http_build_query([
                'secret' => config('services.google.recaptcha3.secret'),
                'response' => $value
            ])
        );
        $client = new Client();
        $response = $client->get($url);
        $body = (array)json_decode((string)$response->getBody(), true);

        if (!$body['success']) {
            \Log::warning(sprintf('[ReCaptcha] %s: Invalid Response!', $value), $body);
            return false;
        }

        $result = $body['score'] > $score;
        if (!$result) {
            \Log::warning(
                sprintf('[ReCaptcha] %s: Invalid Score!', $value),
                ['config' => $score, 'response' => $body]
            );
        }
        return $result;
    }

}
