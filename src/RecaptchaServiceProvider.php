<?php

namespace ORT\Interactive\Recaptcha3;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use ORT\Interactive\Recaptcha3\Fields\GoogleRecaptcha;
use ORT\Interactive\Recaptcha3\Validation\ReCaptcha;


class RecaptchaServiceProvider extends ServiceProvider{

    public function register()
    {
        $config = config('laravel-form-builder');
        $config['custom_fields'] = $config['custom_fields'] ?? [];
        $config['custom_fields']['recaptcha3'] = GoogleRecaptcha::class;
        config(['laravel-form-builder' => $config]);
    }

    public function boot(){
        Validator::extend('recaptcha3', ReCaptcha::class . '@validate');
    }
}
