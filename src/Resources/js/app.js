$(document).ready(() => {
    let $input = $('input[type="hidden"][name="recaptcha"]');
    if ($input.length) {
        let script = document.createElement('script');
        script.src = 'https://www.google.com/recaptcha/api.js?render='+$input.data('key');
        script.onload = () => {
            grecaptcha.ready(() => {
                $input.each(function () {
                    let $element = $(this);
                    grecaptcha.execute($input.data('key'), $element.data('options')).then((token) => {
                        $element.val(token);
                    });
                });
            });
        };
        document.body.append(script);
    }
});
