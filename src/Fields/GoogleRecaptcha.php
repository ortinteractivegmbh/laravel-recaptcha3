<?php
namespace ORT\Interactive\Recaptcha3\Fields;

use Kris\LaravelFormBuilder\Fields\InputType;
use Kris\LaravelFormBuilder\Form;

class GoogleRecaptcha extends InputType
{

    public function __construct($name, $type, Form $parent, array $options = [])
    {
        $options['attr'] = $options['attr'] ?? [];
        $options['attr']['data-options'] = $options['attr']['data-options'] ?? [];
        $options['attr']['data-options']['action'] = $options['attr']['data-options']['action'] ?? class_basename(get_class($parent));
        $options['attr']['data-options'] = json_encode($options['attr']['data-options']);
        $options['attr']['data-key'] = config('services.google.recaptcha3.key');
        parent::__construct($name, 'hidden', $parent, $options);
    }

    /**
     * @return array
     */
    public function getDefaults()
    {
        return array_merge(
            parent::getDefaults(),
            [
                'rules' => 'required|recaptcha3',
            ]
        );
    }

}
